# jmmc-oival-kubernetes

This repo manages the configuration of oival and oival-beta services.

Choose your mapping of both variant in `blue-green-ingress.yml` to both oival and oival-beta backends and deploy it. 

Then run 
`kustomize build overlay/green | kubectl apply -f -`
or 
`kustomize build overlay/blue | kubectl apply -f -`

Both variant have a RollingUpdate strategy to reduce down time during updates. 

Ok the blue-green model has no big sens here because the application is stateless. However we will stick to a common patern applied in the other `jmmc-*-kubernetes` repo
